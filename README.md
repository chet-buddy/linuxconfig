# Personal Config

## Dev

1. Install [MesloLGS NF](https://github.com/fontmgr/MesloLGSNF) nerd font.
2. Install [Alacritty](https://github.com/alacritty/alacritty) and make it default terminal:
```
sudo update-alternatives --install /usr/bin/x-terminal-emulator x-terminal-emulator /usr/local/bin/alacritty 50 
sudo update-alternatives --config x-terminal-emulator
```
3. Install [Oh My ZSH!](https://ohmyz.sh/).
4. Install [Neovim](https://neovim.io/).
5. Install [Lazygit](https://github.com/jesseduffield/lazygit).
6. Install [tmux](https://github.com/tmux/tmux/wiki/Installing)

Then
```
sudo cp -r nvim $XDG_CONFIG_DIRS/
sudo cp -r alacritty $XDG_CONFIG_DIRS/
sudo cp -r tmux/tmux.conf ~/.tmux.conf
```
Note that tmux an tpm do not work with $XDG_CONFIG_DIRS

## XKB

https://altgr-weur.eu/linux.html

